import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.scss']
})
export class NewApplicationComponent implements OnInit {

  constructor(public router: Router, private route: ActivatedRoute) { }
  countries: any = [];

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.countries = data['countries'];
    });
  }

}
