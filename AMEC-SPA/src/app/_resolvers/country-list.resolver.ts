import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { HttpService } from '../_services/http.service';
//import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CountryListResover implements Resolve<any> {
  constructor(private http: HttpService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.http.getCountries().pipe(
      catchError(error => {
        //this.alertify.error('Problem retrieving data');
        //this.router.navigate(['/order']);
        return of(null);
      })
    );
  }

}