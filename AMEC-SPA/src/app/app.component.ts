import { Component, OnInit } from '@angular/core';
import { HttpService } from './_services/http.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private httpService: HttpService) { }
  ngOnInit() { 
    this.httpService.getCountries().subscribe();
  }
}
