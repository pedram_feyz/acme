import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryListResover } from './_resolvers/country-list.resolver';
import { NewApplicationComponent } from './new-application/new-application.component';
import { SuccessPageComponent } from './success-page/success-page.component';


const routes: Routes = [
  { path: '', redirectTo: 'NewApplication', pathMatch: 'full'},
    { path: 'NewApplication', component: NewApplicationComponent, resolve: {countries: CountryListResover}},
    {
      path: '',
      runGuardsAndResolvers: 'always',
      children: [
        { path: 'success', component: SuccessPageComponent}
      ]
    },
    { path: '**', redirectTo: 'NewApplication', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
