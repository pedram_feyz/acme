﻿using ACME.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Interfaces
{
    public interface IPostCodeRepo
    {
        Task<List<Postcodes>> GetAll();
    }
}
