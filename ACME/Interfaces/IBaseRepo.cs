﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Interfaces
{
    public interface IBaseRepo<T> where T: class
    {
        IQueryable<T> GetAll();
        Task<T> Get(int id);
        Task<T> Create(T entity);
        Task<bool> Delete(T entity);
        Task<bool> SaveAll();
        IQueryable<T> GetQuery();
    }
}
