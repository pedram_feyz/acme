﻿using ACME.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Data
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options):base(options)
        {
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Postcodes> Postcodes { get; set; }
        public DbSet<ApplicationOffer> ApplicationOffers { get; set; }
        public DbSet<State> states { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seeding();
        }
    }
}
