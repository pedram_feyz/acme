﻿using ACME.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Data
{
    public static class Seed
    {
        public static void Seeding(this ModelBuilder modelBuilder)
        {
            var countries = new List<Country>()
            {
                new Country { ID = 1, CountryName= "Australia", CountryCode = "AU" },
                new Country { ID = 2, CountryName= "New Zealand", CountryCode = "NZ" },
                new Country { ID = 3, CountryName= "Antarctica", CountryCode = "AQ" },
                new Country { ID = 4, CountryName= "Argentina", CountryCode = "AR" },
                new Country { ID = 5, CountryName= "Brazil", CountryCode = "BR" },
            };

            modelBuilder.Entity<Country>().HasData(countries);

            var states = new List<State>()
            {
                new State { ID=1, StateName= "Australian Capital Territory", StateCode="ACT", CountryId = 1},
                new State { ID=2, StateName= "New South Wales", StateCode="NSW", CountryId = 1},
                new State { ID=3, StateName= "Northern Territory", StateCode="NT", CountryId = 1},
                new State { ID=4, StateName= "Queensland", StateCode="QLD", CountryId = 1},
                new State { ID=5, StateName= "South Australia", StateCode="SA", CountryId = 1},
                new State { ID=6, StateName= "Tasmania", StateCode="TAS", CountryId = 1},
                new State { ID=7, StateName= "Victoria ", StateCode="VIC", CountryId = 1},
                new State { ID=8, StateName= "Western Australia", StateCode="WA", CountryId = 1},
            };

            modelBuilder.Entity<State>().HasData(states);

            var postcodes = new List<Postcodes>()
            {
                new Postcodes { ID= 50, PCode= "0822", Locality= "ACACIA HILLS", State="NT", Category="Delivery Area"},
                new Postcodes { ID= 3802, PCode= "2601", Locality= "ACTON", State="ACT", Category="Delivery Area"},
                new Postcodes { ID= 5172, PCode= "2850", Locality= "AARONS PASS", State="NSW", Category="Delivery Area"},
                new Postcodes { ID= 7795, PCode= "3737", Locality= "ABBEYARD", State="VIC", Category="Delivery Area"},
                new Postcodes { ID= 10354, PCode= "4613", Locality= "ABBEYWOOD", State="QLD", Category="Delivery Area"},
                new Postcodes { ID= 12392, PCode= "5159", Locality= "ABERFOYLE PARK", State="SA", Category="Delivery Area"},
                new Postcodes { ID= 14504, PCode= "6280", Locality= "ABBA RIVER", State="WA", Category="Delivery Area"},
                new Postcodes { ID= 16305, PCode= "7306", Locality= "ACACIA HILLS", State="TAS", Category="Delivery Area"},
                new Postcodes { ID= 16361, PCode= "7315", Locality= "ABBOTSHAM", State="TAS", Category="Delivery Area"},
                new Postcodes { ID= 16389, PCode= "7320", Locality= "ACTON", State="TAS", Comments= "BURNIE", Category="Delivery Area"},
            };

            modelBuilder.Entity<Postcodes>().HasData(postcodes);
        }
    }
}
