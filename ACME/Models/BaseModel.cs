﻿using ACME.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Models
{
    public class BaseModel : IEntity
    {
        public int ID { get ; set; }
    }
}
