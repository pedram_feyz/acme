﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Models
{
    public class Country: BaseModel
    {
        [Required]
        [MaxLength(100)]
        public string CountryName { get; set; }
        [Required]
        [MaxLength(5)]
        public string CountryCode { get; set; }
        // TODO: I mpodified the design for DB since it is not coorect, how about if we want ot add state for other countries as well in future
        public IEnumerable<State> states { get; set; }
    }
}
