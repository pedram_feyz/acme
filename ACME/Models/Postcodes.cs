﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Models
{
    // TODO: The name of this class should not be plural
    public class Postcodes: BaseModel
    {
        [MaxLength(50)]
        public string PCode { get; set; }
        [MaxLength(50)]
        public string Locality { get; set; }
        [MaxLength(50)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Comments { get; set; }

        [MaxLength(50)]
        public string DeliveryOffice { get; set; }
        [MaxLength(50)]
        public string PreSortIndicator { get; set; }
        [MaxLength(50)]
        public string ParcelZone { get; set; }
        [MaxLength(50)]
        public string BSPnumber { get; set; }
        [MaxLength(50)]
        public string BSPname { get; set; }
        [MaxLength(50)]
        public string Category { get; set; }
    }
}
