﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Models
{
    public class State: BaseModel
    {
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
    }
}
