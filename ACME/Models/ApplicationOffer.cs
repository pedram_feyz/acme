﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Models
{
    public class ApplicationOffer: BaseModel
    {
        [Required()]
        [MaxLength(120)]
        public string FullName { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public int? PostcodesId { get; set; }
        public Postcodes Postcodes { get; set; }
    }
}
