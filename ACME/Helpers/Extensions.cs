﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Helpers
{
    public static class Extensions
    {
        public static void AddApplicationError(this HttpResponse response, string messsage)
        {
            response.Headers.Add("Application-Error", messsage);
            //response.Headers.Add("Access-Control-Expose-Header", "Application-Error");
        }
    }
}
