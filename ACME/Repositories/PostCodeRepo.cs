﻿using ACME.Data;
using ACME.Interfaces;
using ACME.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Repositories
{
    public class PostCodeRepo: BaseRepo<Postcodes> , IPostCodeRepo
    {
        public PostCodeRepo(DataContext context) : base(context)
        {

        }

        public new async Task<List<Postcodes>> GetAll()
        {
            var postCodesQuery = this.GetQuery();
            var postCode = await postCodesQuery.Select(x => x).ToListAsync();

            return postCode;
        }
    }
}
