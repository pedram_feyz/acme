﻿using ACME.Data;
using ACME.Interfaces;
using ACME.Models;
using Microsoft.EntityFrameworkCore;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Repositories
{
    public class CountryRepo: BaseRepo<Country>, ICountryRepo
    {
        public CountryRepo(DataContext context) : base(context)
        {

        }

        public new async Task<List<Country>> GetAll()
        {
            var queryCountry =  this.GetQuery();

            // implementing left join
            var countries = await queryCountry
            .SelectMany(x => x.states.DefaultIfEmpty(), (c, s) => new { Country = c, states = s })
            .Where(x => x.states == null || x.states.CountryId == x.Country.ID)
            .ToListAsync();

            var distinctedCountries = countries.Select(x => x.Country).DistinctBy(x => x.ID).ToList();

            return distinctedCountries;
        }
    }
}
