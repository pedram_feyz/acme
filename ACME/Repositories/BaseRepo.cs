﻿using ACME.Data;
using ACME.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Repositories
{
    public class BaseRepo<T> : IBaseRepo<T> where T : class, IEntity
    {
        private readonly DbSet<T> _dbSetContext;
        private readonly DataContext _context;
        public BaseRepo(DataContext context)
        {
            _context = context;
            _dbSetContext = _context.Set<T>();
        }
        public Task<T> Create(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public async Task<T> Get(int id)
        {
            var entity = await _dbSetContext.FirstOrDefaultAsync(x => x.ID == id);
            return entity;
        }

        public IQueryable<T> GetAll()
        {
            //TODO add pagination
            var entities = _dbSetContext.Select(x => x);
            return entities;
        }

        public IQueryable<T> GetQuery()
        {
            var entity = _dbSetContext.Select(x => x);
            return entity;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
