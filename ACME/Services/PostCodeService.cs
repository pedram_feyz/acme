﻿using ACME.Interfaces;
using ACME.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Services
{
    public class PostCodeService: IPostCodeService
    {
        private readonly IPostCodeRepo _postCodeRepo;
        public PostCodeService(IPostCodeRepo postCodeRepo)
        {
            _postCodeRepo = postCodeRepo;
        }

        public async Task<List<Postcodes>> GetAll()
        {
            var postCodes = await _postCodeRepo.GetAll();
            return postCodes;
        }
    }
}
