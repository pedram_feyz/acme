﻿using ACME.Interfaces;
using ACME.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ACME.Services
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepo _countryRepo;
        public CountryService(ICountryRepo countryRepo)
        {
            _countryRepo = countryRepo;
        }
        public async Task<List<Country>> GetAll()
        {
            var countries = await _countryRepo.GetAll();
            return countries;
        }
    }
}
