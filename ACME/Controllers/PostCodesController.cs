﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACME.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ACME.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostCodesController : ControllerBase
    {
        private readonly IPostCodeService _postCodeService;
        public PostCodesController(IPostCodeService postCodeService)
        {
            _postCodeService = postCodeService;
        }
        // GET: api/PostCodes
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var postCodes = await _postCodeService.GetAll();
            return Ok(postCodes);
        }
    }
}
