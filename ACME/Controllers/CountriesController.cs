﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACME.Data;
using ACME.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ACME.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService _countryService;
        public CountriesController(ICountryService countryService)
        {
            _countryService = countryService;
        }
        // GET: api/Countries
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var countries = await _countryService.GetAll();
            return Ok(countries);
        }
    }
}
