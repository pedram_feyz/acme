﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ACME.Migrations
{
    public partial class rename_CountryId_to_ID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CountryId",
                table: "Countries",
                newName: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Countries",
                newName: "CountryId");
        }
    }
}
