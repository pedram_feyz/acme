﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ACME.Migrations
{
    public partial class seeding2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "CountryId", "CountryCode", "CountryName" },
                values: new object[,]
                {
                    { 1, "AU", "Australia" },
                    { 2, "NZ", "New Zealand" },
                    { 3, "AQ", "Antarctica" },
                    { 4, "AR", "Argentina" },
                    { 5, "BR", "Brazil" }
                });

            migrationBuilder.InsertData(
                table: "Postcodes",
                columns: new[] { "ID", "BSPname", "BSPnumber", "Category", "Comments", "DeliveryOffice", "Locality", "PCode", "ParcelZone", "PreSortIndicator", "State" },
                values: new object[,]
                {
                    { 50, null, null, "Delivery Area", null, null, "ACACIA HILLS", "0822", null, null, "NT" },
                    { 3802, null, null, "Delivery Area", null, null, "ACTON", "2601", null, null, "ACT" },
                    { 5172, null, null, "Delivery Area", null, null, "AARONS PASS", "2850", null, null, "NSW" },
                    { 7795, null, null, "Delivery Area", null, null, "ABBEYARD", "3737", null, null, "VIC" },
                    { 10354, null, null, "Delivery Area", null, null, "ABBEYWOOD", "4613", null, null, "QLD" },
                    { 12392, null, null, "Delivery Area", null, null, "ABERFOYLE PARK", "5159", null, null, "SA" },
                    { 14504, null, null, "Delivery Area", null, null, "ABBA RIVER", "6280", null, null, "WA" },
                    { 16305, null, null, "Delivery Area", null, null, "ACACIA HILLS", "7306", null, null, "TAS" },
                    { 16361, null, null, "Delivery Area", null, null, "ABBOTSHAM", "7315", null, null, "TAS" },
                    { 16389, null, null, "Delivery Area", "BURNIE", null, "ACTON", "7320", null, null, "TAS" }
                });

            migrationBuilder.InsertData(
                table: "states",
                columns: new[] { "ID", "CountryId", "StateCode", "StateName" },
                values: new object[,]
                {
                    { 1, 1, "ACT", "Australian Capital Territory" },
                    { 2, 1, "NSW", "New South Wales" },
                    { 3, 1, "NT", "Northern Territory" },
                    { 4, 1, "QLD", "Queensland" },
                    { 5, 1, "SA", "South Australia" },
                    { 6, 1, "TAS", "Tasmania" },
                    { 7, 1, "VIC", "Victoria " },
                    { 8, 1, "WA", "Western Australia" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "CountryId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "CountryId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "CountryId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "CountryId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 3802);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 5172);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 7795);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 10354);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 12392);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 14504);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 16305);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 16361);

            migrationBuilder.DeleteData(
                table: "Postcodes",
                keyColumn: "ID",
                keyValue: 16389);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "states",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "CountryId",
                keyValue: 1);
        }
    }
}
